PLEASE README SVP

# How to launch the front page of Boats
1. Open a terminal and go to the 'boats' folder.
2. Run in the following order

```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

3. Open a browser at this address [http://127.0.0.1:8000/]()
4. You can create a user and get onto the Boats mainpage.


# Login
## TODO
- [x] set up a sign in interface
- [ ] set up a forgot password interface
- [x] set up a managing user data interface
- [ ] add a profile picture
- [x] add the possibility to change username
- [x] add info about gender etc
- [x] add a about us, contact on main page
- [x] add a stack presentation
- [ ] add the historique
- [x] change the signup and save changes button

# User data
## There are two databases with the data of the User
* The database written with django.contrib.auth.model.Model where you can find the username
and the password and several other pieces of information
* The profile database that can be adapted and set up accordingly to the need of our team
