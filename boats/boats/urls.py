from django.conf.urls import url, include
from django.contrib import admin
from django.apps import apps
login_name = apps.get_app_config('login').verbose_name
app_boats_name = apps.get_app_config('app_boats').verbose_name
alerts_name = apps.get_app_config('app_alerts').verbose_name

urlpatterns = [
    url(r'^', include(('login.urls', login_name), namespace='login')),
    url(r'^boats', include(('app_boats.urls', app_boats_name), namespace='app_boats')),
    url(r'alerts/', include(('app_alerts.urls', alerts_name), namespace='app_alerts'))
]
