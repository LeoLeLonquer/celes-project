from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
import requests
import json
from .stubApiModule import generatePathDico
from django.contrib.auth.decorators import login_required
def getPath(request):
  print('ICI')
  if request.method == 'GET':
    if 'long1' in request.GET and 'lat1' in request.GET and 'long2' in request.GET and 'lat2' in request.GET:
      long1 = float(request.GET.get('long1'))
      lat1 = float(request.GET.get('lat1'))
      long2 = float(request.GET.get('long2'))
      lat2 = float(request.GET.get('lat2'))
      STUB = False
      aStarOption = (request.GET.get('aStar'))
      if STUB:
        dico = generatePathDico(lat1, long1, lat2, long2)
        print(dico)
        return JsonResponse(dico, safe=False)
      else:
        print(long1, lat1, " ; ", long2, lat2)
        if aStarOption == "shortest":
          url = "http://10.20.28.81:5000/astar"
        elif aStarOption == "safer":
          url = "http://10.20.28.81:5000/astarDensity"

        # payload = {'start_long': start_long, 'start_lat': start_lat, 'end_long': end_long, 'end_lat': end_lat}
        payload = {'start_long': str(long1), 'start_lat': str(lat1), 'end_long': str(long2), 'end_lat': str(lat2)}

        r = requests.get(url, params=payload)
        dico = json.loads(r.text)
        return JsonResponse(json.loads(r.text), safe=False)

@login_required
def BoatsView(request):
  user=request.user
  print(user)
  return render(request, 'map.html',locals())


