from django.conf.urls import url, include
from . import views

urlpatterns = [
    url('getPath', views.getPath),
    url(r'^$', views.BoatsView, name='boats'),
]
