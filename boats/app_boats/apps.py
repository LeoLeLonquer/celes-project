from django.apps import AppConfig


class AppBoatsConfig(AppConfig):
    name = 'app_boats'
