def generatePathDico(lat1,long1,lat2,long2,nbPoint=10):
    print(long1,lat1,long2,lat2)
    weight=sum([long1,lat1,long2,lat2])
    coordinateList=[]
    coordinateList.append({"long": long1, "lat": lat1})
    for i in range(nbPoint):
        dico=createRandomCoordinate()
        coordinateList.append(dico)
    coordinateList.append({"long": long2, "lat": lat2})
    dico={}
    dico['weight']=weight
    dico['path']=coordinateList
    return dico

import random
def createRandomCoordinate():
    lat=random.randint(-90,90)
    lng=random.randint(-180,180)
    return {"long":lng,"lat":lat}

if __name__=="__name__":
    for line in stub_api():
        print(line)