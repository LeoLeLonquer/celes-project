var optionPirate='optionPirate';
var optionTsunami='optionTsunami';
var optionIceberg='optionIceberg';
var optionMeteo='optionMeteo';
var optionSOS='optionSOS';

mapRenderer=new MapperRenderer();
cronCT= new CronController(mapRenderer);

//Controller des tâches cron qui récupèrent les données
function CronController(mapRenderer){
    self=this;
    this.cronTask={};
    this.mapRenderer=mapRenderer; // Modele des alertes
    this.addTask= function(type){
        this.getAlert(type);
        this.cronTask[type]=window.setInterval(function(){self.getAlert(type);}, 2000);
    };
    this.removeTask=function(type){
        clearInterval(this.cronTask[type]);
        delete this.cronTask[type];
    };

    this.getAlert=function(type){
        var xhr = new XMLHttpRequest();
        xhr.responseType="json";
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
                var result=xhr.response;
                //                console.log(result.points);
                self.mapRenderer.updateModel(type,result.points);
            }
        };
        xhr.open("GET", config.urls.listAlert+"?type="+type, true);
        xhr.send(null);
    }
};

function MapperRenderer(){
    this.map=map;
    this.displayed=false;

    // FUNCTION
    this.optionToDisplay={}
    this.view={};
    this.optionToDisplay[optionPirate]=false;
    this.optionToDisplay[optionTsunami]=false;
    this.optionToDisplay[optionIceberg]=false;
    this.optionToDisplay[optionMeteo]=false;
    this.optionToDisplay[optionSOS]=false;
    this.view[optionPirate]={};
    this.view[optionTsunami]={};
    this.view[optionIceberg]={};
    this.view[optionMeteo]={};
    this.view[optionSOS]={};


    this.cleanMap=function(){
        for (option in this.optionToDisplay){
            if (this.optionToDisplay[option]==true){  // ils sont affichées on efface
                this.setMapByOption(option,null);
                this.optionToDisplay[option]=false;
                cronCT.removeTask(option);
            }
        }
        this.displayed=false;
    }

    this.display=function(){
        this.displayed=true;
        $('#multipleSelect').trigger("change");
    }

    this.setOption=function(option,boolean){
        if (this.displayed){
            if (this.optionToDisplay[option]==false && boolean==true){
                //                console.log('on affiche'+option);
                cronCT.addTask(option);
                this.setMapByOption(option,map);
            }
            else if(this.optionToDisplay[option]==true && boolean==false){
                this.setMapByOption(option,null);
                cronCT.removeTask(option);
            }
            this.optionToDisplay[option]=boolean;
        }

        //        if (boolean){
        //            this.setMapByOption(option,map);
        //        }
        //        else{
        //            this.setMapByOption(option,null);
        //        }
    }

    // SetMap d'une option dans la vue
    this.setMapByOption=function(option,map){
        for (idPoint in this.view[option])  {
            this.view[option][idPoint].setMap(map);
        }
    }

    // SetMap de tous les filtres
    this.setMapFilters=function(map){
        for (option in this.optionToDisplay){
            if (this.optionToDisplay[option]==true){
                this.setMapByOption(option,map);
            }
            else  {
                this.setMapByOption(option,map);
            }
        }
    }

    this.updateModel=function(option,points){
        for (var i=0;i<points.length;i++){
            id=points[i]['id']
            if (id in this.view[option]){
                //                console.log('mise à jour'+id);
            }
            else{
                this.view[option][id]=pointToMarker(points[i]['point'],option);
                this.view[option][id].setMap(map);
            }
        }
    }
}

multipleSelect=document.getElementById('multipleSelect');
$('#multipleSelect').change(function() {
    for (var i=0, iLen=multipleSelect.length; i<iLen; i++) {
        opt = multipleSelect[i];
        if (opt.selected) {
            mapRenderer.setOption(opt.id,true);
        }
        else {
            mapRenderer.setOption(opt.id,false);
        }
    }
});
$('#displayFilter').change(function() {
    if (this.checked){
        mapRenderer.display();
    }
    else{
        mapRenderer.cleanMap();
    }

});

//function AlertModel(){
//    this.filterList=[];
//
//    this.update=function(type,points){
//        this[type]=points
//    };
//
//    this.listener=[];
//    this.addListener=function(){
//        console.log('Listener added');
//    }
//    this.notify=function(){
//        console.log('notify');
//    }
//}



var iconUrlByOption={};
iconUrlByOption[optionPirate]="http://maps.google.com/mapfiles/kml/pal3/icon34.png"
iconUrlByOption[optionSOS]="http://maps.google.com/mapfiles/kml/pal3/icon41.png"
iconUrlByOption[optionMeteo]="http://maps.google.com/mapfiles/kml/pal4/icon42.png"
iconUrlByOption[optionTsunami]="http://maps.google.com/mapfiles/kml/shapes/water.png"
iconUrlByOption[optionIceberg]="http://maps.google.com/mapfiles/kml/pal4/icon52.png"

function pointToMarker(point,optionType){
    coord=point['latLng'];
    //    console.log(coord);
    var marker = new google.maps.Marker({
        position:  new google.maps.LatLng(coord[0],coord[1]) ,
        icon:iconUrlByOption[optionType] ,
    });
    var infoWindow=buildInfoWindow(point,optionType);
    marker.addListener('click', function() {
        infoWindow.open(map, marker);
    });
    return marker;
};

function buildInfoWindow(point,optionType){
//    var date = new Date(parseFloat(point['time'])*1000);
//    var dateToString='date : '+ date.getMonth()+1+'/'+date.getDate()+' time '+date.getHours()+':'+date.getMinutes();
//    dateToString=date.toISOString();
    var contentString = '<div class="content">'+
        '<img src="'+
        iconUrlByOption[optionType]+
        '"  class="img-rounded">'+
        '<h2>'+
        optionType.split("option").join("")+
        '</h2>'+
        '<h4> By '+
        point['user']+
        '</h4>'+
        '<h4>'+
        '('+point['time']+
        ')'+
        '</h4>'+
        '<h4> Lat'+
        point['latLng'][0]+
        ' ; Long '+
        point['latLng'][1]+
        '</h4>'+
        '<p>'+
        point['commentary']+
        '</p>'+
        '</div>';
    //    console.log(contentString);
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    return infowindow;

}
function coordListToPointList(coordList){

    var pointList=[];
    for (var i = 0; i < coordList.length; i++) {
        var marker = new google.maps.Marker({
            position:  new google.maps.LatLng(coordList[i][0],coordList[i][1]) ,
            icon:"http://maps.google.com/mapfiles/kml/pal3/icon34.png" ,
        });
        pointList.push(marker);
    }
    return pointList;
}
function generateFilter(nbPoint){
    var liste={};
    for (var i = 0; i < nbPoint; i++) {
        point=[Math.random()*180-90,Math.random()*360-180]

        var marker = new google.maps.Marker({
            position:  new google.maps.LatLng(coord[0],coord[1]) ,
            icon:"http://maps.google.com/mapfiles/kml/pal3/icon34.png" ,
        });
        liste[i]=marker;
    }
    return liste;
};
