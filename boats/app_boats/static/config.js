var DEBUG=false;
if (DEBUG){
    var baseUrl="http://127.0.0.1:8000/";
    var config={
        urls:{
            getPath:baseUrl+"boats/getPath",
            pushAlert:baseUrl+"alerts/push",
            listAlert:baseUrl+"alerts/list",
            listHeatmap:baseUrl+"alerts/heatmap"
        }
    }
}
else{
    var baseUrl="http://10.20.28.53:8000/";
    var config={
        urls:{
            getPath:baseUrl+"boats/getPath",
            pushAlert:baseUrl+"alerts/push",
            listAlert:baseUrl+"alerts/list",
            listHeatmap:baseUrl+"alerts/heatmap"
        }
    }
}

