
//Recupère les élements sur la page
var depart=document.getElementById("departInput");
var destination=document.getElementById("destinationInput");
var button=document.getElementById("getPath");

button.addEventListener("click", function(){
    //    console.log("Depart:"+depart.value);
    coord1=stringToCoordinate(depart.value);
    //    console.log("Destination:"+destination.value);
    coord2=stringToCoordinate(destination.value);
    params=coordToGetParams(coord1,coord2,document.getElementById("astarOption").checked)
    //    console.log(config.urls.getPath+params);
    var xhr = new XMLHttpRequest();
    xhr.responseType="json";
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            //            console.log(xhr.response);
            //            alert(xhr.response.weight);
            path=xhr.response.path;
            //            console.log(path);
            plotPath(path);
        }
    };
    xhr.open("GET", config.urls.getPath+params, true);
    xhr.send(params);
});

// Submit Alert
var alertButton=document.getElementById("sendAlertButton");
alertButton.addEventListener('click',function(){
    var formAlert=document.getElementById("alertForm");
    //    console.log(stringToCoordinate(formAlert.alertPositionInput.value));
    var data = {
        "latLng": stringToCoordinate(formAlert.alertPositionInput.value),
        "commentary":formAlert.commentary.value,
        "option":formAlert.typeAlerte.value,
        "userName":userName
    };

    formAlert.commentary.value='';
    formAlert.alertPositionInput.value='';

    var xhr = new XMLHttpRequest();
    xhr.responseType="json";
    xhr.open("POST", config.urls.pushAlert, true);
    console.log("param",data)
    //    xhr.open("GET", "http://localhost:8000/roads/list"+params, true);
    xhr.send(JSON.stringify(data));
    alert('Alert sent');
});


function stringToCoordinate(string) {
    var coord=string.split(",");
    return (coord)
}

function coordToGetParams(coord1,coord2,aStarOption) {
    var params="?lat1="+coord1[0];
    params+="&long1="+coord1[1];
    params+="&lat2="+coord2[0];
    params+="&long2="+coord2[1];
    if (aStarOption){
        params+="&aStar="+"shortest";
    }
    else{
        params+="&aStar="+"safer";
    }
    return (params)
}
