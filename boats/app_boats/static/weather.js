
function buildWeatherUrl(coord){
    var url="http://api.worldweatheronline.com/premium/v1/weather.ashx?key=60639d019e824666869182444181401&q="+coord[0]+","+coord[1]+"&format=json";
    return url;
}

var meteo=document.getElementById("meteo");
var weatherImg;
var weatherTemperature;
function getTemperature(coord)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
            var response=JSON.parse(xmlHttp.responseText);
            temp_C=response.data.current_condition[0].temp_C;
            weatherUrl=response.data.current_condition[0].weatherIconUrl[0].value;
            winddirDegree=parseInt(response.data.current_condition[0].winddirDegree);
            if (weatherImg==null){
                weatherImg= document.createElement("img");
                weatherTemperature= document.createElement("p");
                weatherTemperature.innerHTML=temp_C+"°C";
                weatherImg.setAttribute("src", weatherUrl);
                weatherImg.style.width="42px";
                meteo.appendChild(weatherImg);
                meteo.appendChild(weatherTemperature);
            }
            else{
                weatherImg.setAttribute("src", weatherUrl);
                weatherTemperature.innerHTML=temp_C+"°C";
            }
            plotWindDirection(coord,winddirDegree);
        }
    }
    var url=buildWeatherUrl(coord)
    //    console.log(url);
    xmlHttp.open("GET", url, true); // true for asynchronous
    xmlHttp.send(null);
}

var marker;
function plotWindDirection(coord,rotation){
    if (marker==null){
        var icon = {
            path:google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            strokeColor: 'red',
            strokeWeight: 2,
            scale: 4,
            rotation:rotation
        };
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(coord[0], coord[1]),
            icon:icon,
            map:map
        });
    }
    marker.icon.rotation=rotation;
    marker.setMap(null);
    marker.position=new google.maps.LatLng(coord[0], coord[1]);
    marker.setMap(map);
}

//console.log('weater');
//
//function buildWeatherUrl(coord){
//    var url="http://api.wunderground.com/api/51fe4baedb0fcac5/conditions/q/"+coord[0]+","+coord[1]+".json";
//    return url;
//}
//
//function getTemperature(coord)
//{
//    var xmlHttp = new XMLHttpRequest();
//    xmlHttp.onreadystatechange = function() { 
//        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
//            var response=JSON.parse(xmlHttp.responseText);
//            //            console.log(response);
//            console.log(response.current_observation.temp_c);
////            alert("Temperature "+response.current_observation.temp_c)
//        }
//    }
//    var url=buildWeatherUrl(coord)
//    console.log(url);
//    xmlHttp.open("GET", url, true); // true for asynchronous 
//    xmlHttp.send(null);
//}
//
