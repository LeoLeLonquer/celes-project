var heatMap;
function getHeatMap(type){
    var xhr = new XMLHttpRequest(); 
    xhr.responseType="json";
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4 && (xhr.status == 200 || xhr.status == 0)) {
            var heatmapData = [];
            var result=xhr.response;
            console.log('faire attention au sens latlng');
            for(var i=0; i<result.points.length;i++){
                latLng=result.points[i][0]
                //                console.log(latLng);
                heatmapData.push(new google.maps.LatLng(latLng[0], latLng[1]));
            }
            heatMap = new google.maps.visualization.HeatmapLayer({
                data: heatmapData,
                dissipating :true,
                radius:30,
                //        maxIntensity:1
            });
            heatMap.setMap(map);
        }
    };
    xhr.open("GET", config.urls.listHeatmap+"?type="+type, true);
    xhr.send(null);
};


var heatMap;
$('#displayHeatmap').change(function() {
    if (this.checked){
        console.log("Affichage Heatmap");
        getHeatMap(document.getElementById('typeHeatMap').value)
    }
    else{
        heatMap.setMap(null);
    }

});
