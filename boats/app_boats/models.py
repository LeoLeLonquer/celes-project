from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from login.models import Profile


class Direction(models.Model):
    user = models.ForeignKey('login.Profile',
                            on_delete=models.PROTECT)
    start = models.CharField(max_length=100)
    end = models.CharField(max_length=100)
    date = models.DateTimeField(auto_now_add=True, auto_now=False,
                                verbose_name="Date de parution")
