from random import random, choice
import string
import time
def generatePoints(nbPoints):
    latLngList=[]
    for i in range(nbPoints):
        dico={}
        dico['id']=i
        dico['point']={}
        dico['point']['latLng']=(round(random()*180-90,2),round(random()*360-180,2))
        dico['point']['commentary']=''.join(choice(string.ascii_uppercase + string.digits) for _ in range(20))
        dico['point']['user']='Jean Dominique'
        dico['point']['time']=time.time()
        latLngList.append(dico)
    return latLngList

if __name__=="__main__":
    list=generatePoints(10)
    print(list)