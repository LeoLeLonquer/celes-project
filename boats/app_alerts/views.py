from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json
from .alertDB_connector import postAlert, getAlert,getDensity

STUB=False

@csrf_exempt
def pushAlert(request):
    if request.method == "POST":
        data = json.loads(request.body)
        print(data)
        postAlert(data)
    return HttpResponse('')

from .alerteGenerator import generatePoints
def getAlertFilter(request):
    print('get Filter')
    if request.method == "GET":
        type=request.GET['type']
        print(type)
        # density=request.GET['density']
        dico={}
        if STUB :
            nbPoint=2
            # dico['points']=generatePoints(nbPoint)
        else :
            dico['points']=getAlert(type)
    return JsonResponse(dico,safe=False)

def getHeatMap(request):
    if request.method == "GET":
        type=request.GET['type']
        dico={}
        dico['points']=getDensity(type)
    return JsonResponse(dico,safe=False)
