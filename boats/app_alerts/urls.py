from django.conf.urls import url
from . import views

urlpatterns = [
    url('list', views.getAlertFilter),
    url('push', views.pushAlert),
    url('heatmap',views.getHeatMap)
]
