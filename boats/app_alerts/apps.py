from django.apps import AppConfig


class AppAlertsConfig(AppConfig):
    name = 'app_alerts'
