import requests
import json
baseURL="http://10.20.28.80:5000"
optionToType={}
optionToType['optionPirate']="pirate"
optionToType['optionTsunami']="tsunami"
optionToType['optionIceberg']="iceberg"
optionToType['optionMeteo']="meteo"
optionToType['optionSOS']="sos"

# typeToOption={}
# typeToOption['pirate']="optionPirate"
# typeToOption['']=
# typeToOption['']=
# typeToOption['']=
# typeToOption['']=

def postAlert(inputData):
    userName = "None"
    data = {}
    data['long']=inputData['latLng'][1]
    data['lat']=inputData['latLng'][0]
    data['user']=inputData['userName']
    data['type']=optionToType[inputData['option']]
    data['comment']=inputData['commentary']
    print("data envoyée",data)
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    r = requests.post(baseURL+"/alerts/add", data=json.dumps(data), headers=headers)

# pushAlert("optionPirate",[10,20],commentary="des pirates !!!",userName="Denis")

def generatePoints(nbPoints):
    latLngList=[]
    for i in range(nbPoints):
        dico={}
        dico['id']=i
        dico['point']={}
        dico['point']['latLng']=(round(random()*180-90,2),round(random()*360-180,2))
        dico['point']['commentary']=''.join(choice(string.ascii_uppercase + string.digits) for _ in range(20))
        dico['point']['user']='Jean Dominique'
        dico['point']['time']=time.time()
        latLngList.append(dico)
    return latLngList

def toPoint(pointList):
    dico={}
    dico['id']=pointList[0]
    dico['point']={}
    dico['point']['latLng']=(pointList[3],pointList[4])
    dico['point']['commentary']=pointList[2]
    dico['point']['user']=pointList[6]
    dico['point']['time']=pointList[5]
    return dico

def getAlert(optionAlert):
    r=requests.get(url=baseURL+"/maps/alerts",params={"type":optionToType[optionAlert]})
    response=json.loads(r.text)
    alertList=[toPoint(x) for x in response['alerts']]
    print(alertList)
    return alertList

def getDensity(optionAlert):
    r = requests.get(url=baseURL + "/maps/density", params={"type": optionToType[optionAlert]})
    response = json.loads(r.text)
    return response['centers']

if __name__=="__main__":
    for key in optionToType:
    #     print(key)
        getAlert(key)
    # postAlert()