from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from login.models import Profile

GENDER_CHOICES = ((' ', ' '), ('NA', '--'),
        ('Male', 'Male'), ('Female', 'Female'),
        ('Non-binary', 'Non-binary'), ('Pangender', 'Pangender'),
        )

class SignUpForm(UserCreationForm):
    # Fields that will be saved into auth.User and login.Profile
    email = forms.EmailField(max_length=254,
                             help_text='Required. Inform a valid email address.')
    first_name = forms.CharField(max_length=30,
                                 required=False,
                                 help_text='Optional.')
    last_name = forms.CharField(max_length=30,
                                required=False,
                                help_text='Optional.')
    # Fields that will be saved ONLY into login.Profile
    gender = forms.ChoiceField(choices = GENDER_CHOICES,
                                required=False,
                                label = 'Gender',
                                widget=forms.Select(attrs={'style':'width:150px;'}),
                                help_text = 'Optional',
                                )
    birth_date = forms.DateField(required=False,
                                 help_text='Optional. Format: YYYY-MM-DD')
    bio = forms.CharField(max_length=1000,
                          required=False,
                          widget=forms.Textarea(attrs={'style': 'width:250px; height:150px'}),
                          help_text='Optional. Tell us more about yourself\
                                  (e.g. Credit Card Number, Social Security Number etc.)')

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name',
                 'gender', 'birth_date', 'bio', 'password1', 'password2', )


class ModifyInfoForm(forms.Form):
    # Fields that will be saved into auth.User and login.Profile
    email = forms.EmailField(max_length=254,
                             required=False,
                             help_text='Inform a valid email address.')
    first_name = forms.CharField(max_length=30,
                                 required=False)
    last_name = forms.CharField(max_length=30,
                                required=False)
    # Fields that will be saved ONLY into login.Profile
    gender = forms.ChoiceField(choices = GENDER_CHOICES, 
                                required=False,
                                label = 'Gender',
                                widget=forms.Select(attrs={'style':'width:150px;'}),
                                help_text = 'Optional',)
    birth_date = forms.DateField(required=False,
                                 help_text='Format: YYYY-MM-DD' )
    bio = forms.CharField(max_length=1000,
                          required=False,
                          widget=forms.Textarea(attrs={'style': 'width:250px; height:150px'}),
                          help_text='Tell us more about yourself\
                                  (e.g. Credit Card Number, Social Security Number etc.)')
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name',
                'gender', 'birth_date', 'bio','password', 'newpassword1', 'newpassword2')

