from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from login.forms import SignUpForm, ModifyInfoForm
from django.http import HttpResponseRedirect
from django.urls import reverse

def custom_login(request, template_name):
    _message = 'Please sign in'
    if request.method == 'POST':
        _username = request.POST['username']
        _password = request.POST['password']
        user = authenticate(username=_username, password=_password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('app_boats:boats'))
            else:
                _message = 'Your account is not activated'
        else:
            _message = 'Invalid login, please try again.'
    context = {'message': _message}
    return render(request, template_name, context)

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save() # automatically save username, password,
                               # mail, last_name and first_name
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.email = form.cleaned_data.get('email')
            user.profile.last_name = form.cleaned_data.get('last_name')
            user.profile.first_name = form.cleaned_data.get('first_name')
            user.profile.gender = form.cleaned_data.get('gender')
            user.profile.birth_date = form.cleaned_data.get('birth_date')
            user.profile.bio = form.cleaned_data.get('bio')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('app_boats:boats')
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})

@login_required
def modify_user(request):
    # TODO implement the username and password modification
    # in django.contrib.auth there might be a Password Reset form
    if request.method == 'POST':
        form = ModifyInfoForm(request.POST)
        if form.is_valid():
            user = request.user
            user.refresh_from_db()  # load the profile instance created by the signal
            email = form.cleaned_data.get('email')
            last_name = form.cleaned_data.get('last_name')
            first_name = form.cleaned_data.get('first_name')
            gender = form.cleaned_data.get('gender')
            print(gender)
            birth_date = form.cleaned_data.get('birth_date')
            bio = form.cleaned_data.get('bio')
            if email != '':
                user.profile.email = email
                user.email = email
            if last_name != '':
                user.profile.last_name = last_name
                user.last_name = last_name
            if first_name != '':
                user.profile.first_name = first_name
                user.first_name = first_name
            if gender != ' ':
                user.profile.gender = gender
            if birth_date != None:
                user.profile.birth_date = birth_date
            if bio != '':
                user.profile.bio = bio
            user.save()
            return redirect('login:settings')
    else:
        form = ModifyInfoForm()
    return render(request, 'settings.html', {'form': form})

def homepage(request, template_name):
    return render(request, template_name=template_name)
