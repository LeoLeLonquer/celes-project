from django.contrib.auth import views as auth_views
from django.views.generic.base import TemplateView
from django.conf.urls import url
from django.contrib import admin
from login import views

urlpatterns = [
    url(r'^$', views.homepage, {'template_name':'home.html'}, name='home'),
    url(r'home^$', views.homepage, {'template_name':'home.html'}, name='home'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^login/$', views.custom_login, {'template_name': 'login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'template_name': 'logged_out.html'}, name='logout'),
    url(r'^settings/$', views.modify_user,  name='settings'),
    url(r'^change_password$',
        auth_views.password_change,
        {'template_name':'change_password.html',
            'post_change_redirect':'login:password_reset_done'},
        name='change_password'),
    url(r'^password_reset_done/$',
        TemplateView.as_view(template_name='password_reset_done.html'),
        name='password_reset_done'),
]
